'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {


  it('should automatically redirect to /individual-onboarding when location hash/fragment is empty', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/individual-onboarding");
  });


  describe('individual-onboarding', function() {

    beforeEach(function() {
      browser.get('index.html#!/individual-onboarding');
    });


    it('should render individual-onboarding when user navigates to /individual-onboarding', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/partial for view 1/);
    });

  });


  describe('business-onboarding', function() {

    beforeEach(function() {
      browser.get('index.html#!/business-onboarding');
    });


    it('should render business-onboarding when user navigates to /business-onboarding', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/partial for view 2/);
    });

  });
});
