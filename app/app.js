"use strict";

var myApp = angular.module("myApp", ["ngRoute"]);

myApp.factory("Check", function () {
    return function () {
        return localStorage.getItem("USER_LOGGED_IN");
    };
});

var dev = "http://34.214.153.95:9090";
var staging = "http://35.167.14.111:8080";
var production = "http://52.38.174.165:8080";

myApp.config(function ($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix("!");
    $routeProvider.when("/", {
        templateUrl: "components/homepage/homepage.html",
        controller: "HomePageController as $ctrl"
    }).when("/business-onboarding", {
        templateUrl: "components/business-onboarding/business-onboarding.html",
        controller: "CompanyController as $ctrl"
    }).when("/Homepage", {
        templateUrl: "components/homepage/homepage.html",
        controller: "HomePageController as $ctrl"
    }).when("/business-signin", {
        templateUrl: "components/business-onboarding/business-signin.html",
        controller: "BusinessSigninController as $ctrl"
    }).when("/user-dashboard", {
        templateUrl: "components/company-dashboard/company-dashboard.html",
        controller: "CompanyDashboardController as $ctrl"
    }).when("/secure/company-verification-step-non-signatory-form", {
        templateUrl: "components/company-verification/company-verification-step-non-sig-form.html",
        controller: "CompanyVerificationController as $ctrl"
    }).when("/secure/company-verification-document-confirm", {
        templateUrl: "components/company-verification/company-verification-step-document-upload-confirm.html",
        controller: "CompanyVerificationController as $ctrl"
    }).when("/coming-soon", {
        templateUrl: "components/common/coming-soon.html",
        controller: "ComingSoonController as $ctrl"
    }).when("/secure/business-bulk-upload", {
        templateUrl: "components/company-payroll-accounts/company-payroll-accounts.html",
        controller: "CompanyPayrollController as $ctrl"
    }).when("/secure/customer-onboarding", {
        templateUrl: "components/customer-onboarding/customer-onboarding.html",
        controller: "CustomerOnboardingController as $ctrl"
    }).when("/partners", {
        templateUrl: "components/partner/partner-signin.html",
        controller: "PartnerController as $ctrl"

    }).when("/services", {
        templateUrl: "components/services/services.html",
        controller: "ServiceController as $ctrl"
    }).when("/404", {
        templateUrl: "components/common/404.html",
        controller: "404Controller as $ctrl"
    }).otherwise({
        redirectTo: "/partners"
    });
}).run(function (Check, $location) {

    var loggedIn = Check();
    var isSecureURL = $location.$$path.match(/^\/secure\/*/);
    //
    // if (!loggedIn) {
    //     alert("hi");
    //     $location.path("/business-signin");
    // } else if (loggedIn) {
    //     if ($location.$$path === "/business-signin") {
    //         $location.path("/");
    //     }
    // }
});

myApp.directive("fileModel", ["$parse", function ($parse) {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind("change", function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);


// var ApiUrl = dev;
var ApiUrl = staging;
// var ApiUrl = production;


myApp.constant("ApiUrl", ApiUrl);