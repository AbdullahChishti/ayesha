"use strict";

describe("myApp.individual-onboarding module", function () {

    beforeEach(module("myApp.individual-onboarding"));

    describe("individual-onboarding controller", function () {

        it("should ....", inject(function ($controller) {
            //spec body
            var view1Ctrl = $controller("View1Ctrl");
            expect(view1Ctrl).toBeDefined();
        }));

    });
});