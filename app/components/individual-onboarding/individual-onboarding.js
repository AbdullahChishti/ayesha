"use strict";

myApp.controller("indexController", ["$scope", "$http", "ApiUrl", function ($scope, $http, ApiUrl) {

    var self = this;
    self.individual = {};
    self.step = 1;
    self.individual.entityType = "INDIVIDUAL";

    self.step1 = function () {
        console.log(self.individual);
        $http({
            method: "POST",
            url: ApiUrl + "/webservices/signUp/individual/email-name-password",
            headers: {"Content-Type": "application/json"},
            params: {
                "email": self.individual.myEmail,
                "first-name": self.individual.firstName,
                "last-name": self.individual.lastName,
                "password": self.individual.myPassword
            }
        }).then(function mySuccess(response) {
            self.individual.uuid = response.data.data;
            alert(self.individual.uuid);
            self.step = 2;

        }, function myError(response) {
            alert("There is an error : " + response.data.message);
        });
    };

    self.step2 = function () {
        $http({
            method: "POST",
            url: ApiUrl + "/webservices/signUp/mobile-no",
            headers: {"Content-Type": "application/json"},
            params: {
                "mobile-no": self.individual.mobileNumber,
                "country-code": self.individual.countryCode,
                "entity-type": self.individual.entityType,
                "uuid": self.individual.uuid
            }
        }).then(function mySuccess(response) {
            self.step = 3;

        }, function myError(response) {
            alert("There is an error : " + response.data.errors[0].message);
        });

    };

    self.step3 = function () {
        self.OTP = self.individual.otp1 + "" + self.individual.otp2 + "" + self.individual.otp3 + "" + self.individual.otp4 + "";
        alert(self.OTP);
        $http({
            method: "POST",
            url: ApiUrl + "/webservices/signUp/verify-otp",
            headers: {"Content-Type": "application/json"},
            params: {
                "otp": self.OTP,
                "entity-type": self.individual.entityType,
                "uuid": self.individual.uuid
            }
        }).then(function mySuccess(response) {
            console.log(response.data);
            self.step = 3;

        }, function myError(response) {
            alert("There is an error : " + response.data.errors[0].message);
        });

    };

    self.ResendOtp = function () {
        $http({
            method: "GET",
            url: ApiUrl + "/webservices/signUp/resend-otp",
            headers: {"Content-Type": "application/json"},
            params: {
                "mobile-no": "92" + self.individual.mobileNumber,
                "entity-type": self.individual.entityType,
                "uuid": self.individual.uuid
            }
        }).then(function mySuccess(response) {
            alert("OTP has been successfully resent.");

        }, function myError(response) {
            alert("There is an error : " + response.data.errors[0].message);
        });
    };
}]);