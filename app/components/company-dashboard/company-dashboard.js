"use strict";

myApp.controller("CompanyDashboardController", ["$scope", "$location", "$http", "ApiUrl", "$route", "$timeout",
    function ($scope, $location, $http, ApiUrl, $route, $timeout) {

        var self = this;
        $scope.userName = localStorage.getItem("USER_FULL_NAME");
        $scope.people = ["less then 5", "less than 10", "less than 20"];
        $scope.houseSize = ["3 marla", "5 marla", "10 marla", "1 kanal"];
        $scope.genders = ["Male","Female"];
        $scope.religion = ["Islam","Christian" ,"Hindu"];
        $scope.Age = ["less than 25","less than 30" ,"less than 40"];
        $scope.duration = ["Full time","Part time"];
        $scope.salary = ["less than Rs.5,000","less than Rs.10,000", "less than Rs.5,000" ,"less than Rs.20,000"];
        $scope.UserType = localStorage.getItem("ENTITY_TYPE");

        $scope.showStartCompanyVerification = function () {
            $location.path("/secure/company-verification-step-1");
        };

        $scope.direct_to_AddEmployee = function () {
            $location.path('/secure/employee-onboarding');
        };

        $scope.direct_to_AddDependent = function () {
            $location.path("/secure/dependent-onboarding");
        };
    }]);