"use strict";

myApp.controller("DependentOnboardingController",
    ["$scope", "$http", "$location", "ApiUrl", "$rootScope", "$timeout",
        function ($scope, $http, $location, ApiUrl, $rootScope, $timeout) {
            var self = this;
            $scope.SubmitButtonText = "Submit";
            $scope.ButtonText = "Next";
            self.dependent = [].fill(0);
            $scope.names = ["M", "F"];
            self.step = 1;

            //initialing all the variables so exceptions don't appear
            self.dependent.FullName = "";
            self.dependent.DOB = "";
            self.dependent.FullName = "";
            self.dependent.PassportNumber = "";
            self.dependent.emailAddress = "";
            self.dependent.Nationality = "";
            $scope.formattedNumber = "";
            self.uuid = localStorage.getItem("USER_UUID");
            var DocumentType = {
                "EmiratesId": 1,
                "Passport": 2,
                "UaeVisa": 3,
                "TradeLicenseFile": 4,
                "Video": 5,
                "ArticleOfIncorporation": 6,
                "selfieVideo": "8"
            };


            // first http call to get sub acounts types
            $http({
                method: "GET",
                url: ApiUrl + "/webservices/refdata/sub-accounts-type"
            }).then(function mySuccess(response) {
                $scope.UserData = response.data.data;
            }, function myError(response) {
                alert("No");
            });

            /*     **************************
                Function calls start here
                ****************************/

            // When the user selects a user type , it comes here
            self.Goto_NextStep = function () {
                self.step = 2;
            };

            //this func is called to verify user's mobile number
            self.Verify_Employee_MobileNo = function () {

                //formatting the number
                $scope.mob = $("#mobileNumber").val();
                if ($scope.mob || $scope.mob.trim().length > 0) {
                    if (/^[0-9]{1,20}$/.test($scope.mob)) {
                        $scope.formattedNumber = "+" + self.dependent.countryCode + parseInt($scope.mob);
                    }
                }
                // second http call to verify mobile number of the employee
                $http({
                    method: "POST",
                    url: ApiUrl + "/webservices/utility/verify-mobile",
                    params: {
                        "mobile-no	": $scope.formattedNumber,
                        "entity-type": "DEPENDENT"
                    }
                }).then(function mySuccess(response) {
                    self.step = 3;
                }, function myError(response) {
                    alert("No");
                });


                self.step = 2;


            };

            self.Verify_Employee_MobileNo = function () {
                $scope.ButtonText = "Please wait..";

                $scope.mob = $("#mobileNumber").val();
                if ($scope.mob || $scope.mob.trim().length > 0) {
                    if (/^[0-9]{1,20}$/.test($scope.mob)) {
                        $scope.formattedNumber = "+" + self.dependent.countryCode + parseInt($scope.mob);
                    }
                }
                $http({
                    method: "GET",
                    url: ApiUrl + "/webservices/utility/verify-mobile",
                    headers: {
                        "Content-Type": undefined
                    },
                    params: {
                        "mobile-no": $scope.formattedNumber,
                        "entity-type": "EMPLOYEE"
                    }
                }).then(function mySuccess(response) {
                    self.step = 3;
                    $scope.ButtonText = "Next";
                }, function myError(response) {
                    $scope.ButtonText = "Try again";
                    alert(response.data.errors[0].message);
                    // $scope.FailiureMessage = response.data.errors[0].message;
                });
            };

            self.Verify_Employee_Email = function () {
                $http({
                    method: "POST",
                    url: ApiUrl + "/webservices/signUp/verify-email",
                    params: {
                        "email": self.dependent.emailAddress,
                        "entity-type": "EMPLOYEE"
                    }
                }).then(function mySuccess(response) {
                    self.step = 4;
                    $scope.ButtonText = "Next";
                }, function myError(response) {
                    alert(response.data.errors[0].message);
                    // $scope.FailiureMessage = response.data.errors[0].message;
                });
            };


            self.More_Employee_Info = function () {
                self.step = 5;
            };
            self.Back_To_DependentFlow = function () {
                location.reload();
            };

            self.Submit__DependentData = function () {
                if (!self.dependent.DOB) {
                    $scope.FailiureMessage = "Enter a valid date of birth"
                }
                if ($scope.formattedNumber || self.FormattedDate
                    || self.dependent.FullName
                    || self.dependent.PassportNumber
                    || self.dependent.emailAddress
                    || self.uuid || self.dependent.DOB) {
                    $scope.SubmitButtonText = "Processing .. Please wait";
                    self.FormattedDate =
                        self.dependent.DOB.getMonth() + 1 + "/" +
                        self.dependent.DOB.getDate() + "/" +
                        self.dependent.DOB.getYear();


                    // This uploads the EID
                    var fileData = new FormData();
                    fileData.append("files", self.EmiratesId);
                    // fileData.append("data", self.Passport);

                    // This uploads the passport
                    // var PassportData = new FormData();
                    // var Indata = {'product': self.Passport, 'product2': self.EmiratesId};
                    $http.post(ApiUrl + "/webservices/individual/dependant-account", fileData, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined},
                        params: {
                            "eid-files": self.EmiratesId,
                            "passport-files	": self.Passport,
                            "dependant-type": "1",
                            "name": self.dependent.FullName,
                            "dob": self.FormattedDate,
                            "gender": self.dependent.gender,
                            "mobile": $scope.formattedNumber,
                            "nationality": "emirati",
                            "expiry": "",
                            "email": self.dependent.emailAddress,
                            "passport-no": self.dependent.PassportNumber,
                            "uuid": self.uuid
                        }
                    })
                        .success(function (response) {
                            $scope.SuccessMessage = "Your sub account has been successfully created";
                            $scope.FailiureMessage = "";

                        })
                        .error(function (response) {
                            if (response.status = 500 && response.status == '500' && response.status == "500") {
                                $scope.FailiureMessage = "Server error :" + response.message;
                            }
                            $scope.FailiureMessage = response.errors[0].message;
                            $scope.SuccessMessage = "";

                        });


                    // $http({
                    //     method: "POST",
                    //     url: ApiUrl + "/webservices/individual/dependant-account",
                    //     transformRequest: angular.identity,
                    //     headers: {
                    //         "Content-Type": undefined
                    //     },
                    //     params: {
                    //         "eid-files": self.EmiratesId,
                    //         "passport-files	": self.Passport,
                    //         "uuid": self.uuid,
                    //         "dependant-type": "DEPENDENT",
                    //         "name": self.dependent.FullName,
                    //         "dob": self.FormattedDate,
                    //         "gender": self.dependent.gender,
                    //         "mobile": $scope.formattedNumber,
                    //         "nationality": "emirati",
                    //         "expiry": "",
                    //         "email": self.dependent.emailAddress,
                    //         "passport-no": self.dependent.PassportNumber
                    //     },
                    //     data: fileData
                    // }).then(function mySuccess(response) {
                    //     $scope.SubmitButtonText = "Success!";
                    //     $scope.SuccessMessage = "Your sub account has been added successfully"
                    //
                    // }, function myError(response) {
                    //     $scope.SubmitButtonText = "Something went wrong";
                    //     $scope.FailiureMessage = response.data.errors[0].message;
                    //
                    // })

                }
                else {
                    $scope.FailiureMessage = "Something went wrong!";

                }
            };


            /*  *********************************
              file uploading module starts here
              ***********************************/


            /*   ***************************************
               Code for country code and flag
               *****************************************/
            $("#mobileNumber").intlTelInput({
                setCountry: "uae",
                preferredCountries: ["ae", "sa"],
                separateDialCode: true
            });

            var countryData = $.fn.intlTelInput.getCountryData(),
                telInput = $("#mobileNumber");
            telInput.intlTelInput({
                utilsScript: "./../assets/js/utils.js" // just for formatting/placeholders etc
            });

            $("#mobileNumber").on("countrychange", function (e, countryData) {
                self.dependent.countryCode = countryData.dialCode;
            });

            $("#mobileNumber").on("change", function () {
                self.dependent.countryCode = $("#mobileNumber").intlTelInput("getSelectedCountryData").dialCode;
            });


            /*****************************
             HTTP CALL TO API STARTS HERE
             *****************************/

        }])
;