"use strict";

myApp.controller("CustomerOnboardingController", ["$scope", "$http", "$location", "ApiUrl", "$rootScope",
    function ($scope, $http, $location, ApiUrl, $rootScope) {

        var DocumentType = {
            "EmiratesId": 1,
            "Passport": 2,
            "UaeVisa": 3,
            "TradeLicenseFile": 4,
            "Video": 5,
            "ArticleOfIncorporation": 6,
            "selfieVideo": "8"
        };

        var num;
        var self = this;
        self.customer = {};
        self.step = 0;
        self.customer.entityType = "INDIVIDUAL";
        self.customer.countryCode = "";
        $scope.ButtonText = "JOIN NOW";
        $scope.ErrorMessage = "";
        self.uuid = localStorage.getItem("USER_UUID");
        $scope.UploadButtonText = 'Upload';

        $("#mobileNumber").intlTelInput({
            setCountry: "uae",
            preferredCountries: ["ae", "sa"],
            separateDialCode: true
        });

        self.SkipToDashboard = function () {
            $location.path('/business-signin');
        };

        var countryData = $.fn.intlTelInput.getCountryData(),
            telInput = $("#mobileNumber");
        telInput.intlTelInput({
            utilsScript: "./../assets/js/utils.js" // just for formatting/placeholders etc
        });

        $("#mobileNumber").on("countrychange", function (e, countryData) {
            self.customer.countryCode = countryData.dialCode;
        });

        $("#mobileNumber").on("change", function () {
            self.customer.countryCode = $("#mobileNumber").intlTelInput("getSelectedCountryData").dialCode;
        });

        self.directTo__home = function () {
            $location.path("/business-signin");
        };

        self.directTo__SignIn = function () {
            $location.path("/business-signin");
        };

        self.gotToStep = function (stepNumber) {
            self.step = stepNumber;
        };
        self.showUploadDocumentsConfirmationStep = function () {
            var passportFile = self.passportFile;
            var emiratesIdFile = self.emiratesIdFile;
            var selfieVideo = self.selfieVideo;
            var uuid = localStorage.getItem("USER_UUID");
            var entityType = "INDIVIDUAL";

            $rootScope.passportFileUploaded = false;
            $rootScope.emiratesIdFileUploaded = false;
            var p1, p2, p3;
            if (passportFile) {
                p1 = new Promise(function (resolve, reject) {
                    uploadFileToServer(passportFile, uuid, DocumentType.Passport, entityType, resolve, reject);
                });
                $rootScope.passportFileUploaded = true;
            }
            if (selfieVideo) {
                p2 = new Promise(function (resolve, reject) {
                    uploadFileToServer(selfieVideo, uuid, DocumentType.Video, entityType, resolve, reject);
                });
                $rootScope.selfieVideo = true;
            }
            if (emiratesIdFile) {
                p3 = new Promise(function (resolve, reject) {
                    uploadFileToServer(emiratesIdFile, uuid, DocumentType.EmiratesId, entityType, resolve, reject);
                });
                $rootScope.emiratesIdFileUploaded = true;
            }
        };

        function uploadFileToServer(file, uuid, documentType, entityType, resolve, reject) {
            var fileData = new FormData();
            fileData.append("files", file);
            $scope.UploadButtonText = 'Uploading Document';

            $http({
                method: "POST",
                url: ApiUrl + "/webservices/documents/user-documents",
                transformRequest: angular.identity,
                headers: {"Content-Type": undefined},
                params: {
                    "uuid": self.uuidDocuments,
                    "document-type": documentType,
                    "entity-type": self.customer.entityType
                },
                data: fileData
            }).then(function mySuccess(response) {
                resolve(response);
                self.step = 8;
            }, function myError(response) {
                reject(response);
            });
        }

        self.step1 = function (isValid) {
            $scope.ErrorMessage = "";

            if (isValid) {
                $scope.ButtonText = "Please wait...";
                $http({
                    method: "POST",
                    url: ApiUrl + "/webservices/signUp/verify-email",
                    headers: {"Content-Type": "application/json"},
                    params: {
                        "email": self.customer.myEmail,
                        "entity-type": "INDIVIDUAL"
                    }
                }).then(function mySuccess(response) {
                    $scope.ButtonText = "JOIN NOW";
                    $scope.$emit("MyEvent", {message: 3});
                    self.step = 2;
                }, function myError(response) {
                    $scope.ButtonText = "Try again";
                    if (response.status == 400 && response.data.data == null && response.data.errors[0].code) {
                        self.step = 20;
                        num = response.data.errors[0].message;

                        $http({
                            method: "GET",
                            url: ApiUrl + "/webservices/utility/send-otp",
                            headers:
                                {
                                    "Content-Type": "application/json"
                                },
                            params: {
                                "email": self.company.myEmail,
                                "entity-type": self.company.entityType,
                                "uuid": num
                            }
                        }).then(function mySuccess(response) {
                            if (response) {
                                $scope.formattedNumber = response.data.data;
                            }
                            self.step = 21;

                        }, function myError(response) {
                            console.error(response);
                        });
                    }
                    if (response.data && response.data.errors && response.data.errors[0]) {
                        $scope.ErrorMessage = response.data.errors[0].message;
                    }
                });
            } else {
                console.log("There is some issue with form validation");
            }
        };

        self.step2 = function (isValid) {
            $scope.ErrorMessage = "";

            if (isValid) {
                self.step = 3;
            }
        };

        self.step3 = function (isValid) {
            $scope.ErrorMessage = "";

            if (isValid)
                self.step = 4;
            else {
                console.log("Invalid form data");
            }
        };

        self.step4 = function (isValid) {
            $scope.ErrorMessage = "";

            if (isValid) {
                if (self.customer.myPassword === self.customer.confirmPassword) {
                    self.step = 5;
                }
                else {
                    $scope.ErrorMessage = "Password do not match.";
                }
            }
        };

        self.step5 = function (isValid) {
            $scope.ErrorMessage = "";

            var mob = $("#mobileNumber").val();
            if (mob || mob.trim().length > 0) {
                if (/^[0-9]{1,20}$/.test(mob)) {

                    $scope.formattedNumber = "+" + self.customer.countryCode + parseInt(mob);

                    $scope.ErrorMessage = "";

                    $http({
                        method: "POST",
                        url: ApiUrl + "/webservices/signUp/individual/",
                        headers: {"Content-Type": "application/json"},
                        params: {
                            "email": self.customer.myEmail,
                            "first-name": self.customer.firstName,
                            "last-name": self.customer.lastName,
                            "password": self.customer.myPassword,
                            "mobile-no": $scope.formattedNumber
                        }
                    }).then(function mySuccess(response) {
                        self.customer.uuid = response.data.data;
                        num = response.data.data;
                        self.uuidDocuments = num;
                        self.step = 6;
                    }, function myError(response) {
                        console.error(response);
                        if (response.data && response.data.errors && response.data.errors[0]) {
                            $scope.ErrorMessage = response.data.errors[0].message;
                        }
                    });
                } else {
                    $scope.ErrorMessage = "Invalid mobile number.";
                }
            } else {
                $scope.ErrorMessage = "Mobile no. is required.";
            }
        };

        self.step6 = function (isValid) {
            $scope.ErrorMessage = "";

            if (isValid) {
                $http({
                    method: "POST",
                    url: ApiUrl + "/webservices/signUp/verify-otp",
                    headers:
                        {
                            "Content-Type": "application/json"
                        },
                    params: {
                        "uuid": num,
                        "otp": self.customer.OTP,
                        "entity-type": self.customer.entityType

                    }
                }).then(function mySuccess(response) {
                    console.log('the response is ' + response);
                    $location.path('/business-signin');
                }, function myError(response) {
                    $scope.OtpError = "OTP is invalid. Try again.";
                });
            }

        };

        self.step7 = function () {

        };

        self.step8 = function () {

        };


        self.resendOTP = function () {
            $scope.ErrorMessage = "";

            $http({
                method: "GET",
                url: ApiUrl + "/webservices/utility/resend-otp",
                headers:
                    {
                        "Content-Type": "application/json"
                    },
                params: {
                    "mobile-no": $scope.formattedNumber,
                    "entity-type": self.customer.entityType,
                    "uuid": num
                }
            }).then(function mySuccess(response) {
                self.otpMsg = 1;
            }, function myError(response) {
                self.otpMsg = 2;
                console.error(response);
            });
        };

        self.verifyResumedOTP = function (isValid) {
            $scope.ErrorMessage = "";

            if (isValid) {
                $http({
                    method: "POST",
                    url: ApiUrl + "/webservices/signUp/verify-otp",
                    headers:
                        {
                            "Content-Type": "application/json"
                        },
                    params: {
                        "uuid": num,
                        "otp": self.customer.OTP,
                        "entity-type": self.company.entityType

                    }
                }).then(function mySuccess(response) {

                }, function myError(response) {
                    $scope.OtpError = "OTP is invalid. Try again.";
                });
            }

        };

    }]);