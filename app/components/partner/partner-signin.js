"use strict";

myApp.controller("PartnerController", ["$scope", "$location", "ApiUrl", "$http", function ($scope, $location, ApiUrl, $http) {
    var self = this;

    self.step = 1;
    self.signup = {};
    self.login = {};

    self.direct_to_order = function () {
        $location.path("/services");
    };

}]);
