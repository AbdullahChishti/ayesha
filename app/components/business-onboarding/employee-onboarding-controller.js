"use strict";

myApp.controller("EmployeeOnboardingController",
    ["$scope", "$http", "$location", "ApiUrl", "$rootScope", "$timeout",
        function ($scope, $http, $location, ApiUrl, $rootScope, $timeout) {
            var self = this;
            $scope.SubmitButtonText = "Submit";
            self.dependent = {};
            self.dependent.WPS = "";
            self.dependent.EID = "";
            self.step = 1;
            self.uuid = localStorage.getItem("USER_UUID");
            $scope.names = ["M", "F"];
            $scope.MainText = "Hi there .. Let's add your sub accounts";
            $scope.ButtonText = "Next Step";

            var DocumentType = {
                "EmiratesId": 1,
                "Passport": 2,
                "UaeVisa": 3,
                "TradeLicenseFile": 4,
                "Video": 5,
                "ArticleOfIncorporation": 6,
                "selfieVideo": "8"
            };

            self.Verify_EmployeeNo = function () {

                if (self.dependent.EmployeeNo) {
                    $scope.ButtonText = "Please wait";
                    $http({
                        method: "POST",
                        url: ApiUrl + "/webservices/employee/verify-employee-no",
                        headers: {
                            "Content-Type": undefined
                        },
                        params: {
                            "employee-no": self.dependent.EmployeeNo,
                            "uuid": self.uuid
                        }
                    }).then(function mySuccess(response) {
                        self.step = 2;
                        $scope.ButtonText = "Next Step";

                        // $scope.SubmitButtonText = "Success!";
                        // $scope.SuccessMessage = "Your sub account has been added successfully"

                    }, function myError(response) {
                        // $scope.SubmitButtonText = "Something went wrong";
                        // $scope.FailiureMessage = response.data.errors[0].message;

                    })
                }
                else {
                    alert("Enter a valid employee number");
                }
            };

            self.Verify_Employee_MobileNo = function () {

                $scope.mob = $("#mobileNumber").val();
                if ($scope.mob || $scope.mob.trim().length > 0) {
                    if (/^[0-9]{1,20}$/.test($scope.mob)) {
                        $scope.formattedNumber = "+" + self.dependent.countryCode + parseInt($scope.mob);
                    }
                }
                if ($scope.formattedNumber) {
                    $scope.ButtonText = "Please wait..";
                    $http({
                        method: "GET",
                        url: ApiUrl + "/webservices/utility/verify-mobile",
                        headers: {
                            "Content-Type": undefined
                        },
                        params: {
                            "mobile-no": $scope.formattedNumber,
                            "entity-type": "EMPLOYEE"
                        }
                    }).then(function mySuccess(response) {
                        self.step = 3;
                    }, function myError(response) {
                        alert(response.data.errors[0].message);
                        // $scope.FailiureMessage = response.data.errors[0].message;
                    });
                }
                else {
                    alert("enter a valid phone number")
                }
            };

            self.dependent_MoreInfo = function () {
                self.step = 4;
            };
            self.Back_To_DependentFlow = function () {
                location.reload();
            };

            self.Submit__DependentData = function () {
                if ($scope.formattedNumber || self.FormattedDate
                    || self.dependent.FullName
                    || self.dependent.PassportNumber
                    || self.dependent.emailAddress
                    || self.uuid
                    || self.EmiratesId) {
                    $scope.SubmitButtonText = "Processing .. Please wait";
                    self.FormattedDate =
                        self.dependent.DOB.getMonth() + 1 + "/" +
                        self.dependent.DOB.getDate() + "/" +
                        self.dependent.DOB.getYear();

                    var fileData = new FormData();
                    fileData.append("files", self.EmiratesId);
                    $http({
                        method: "POST",
                        url: ApiUrl + "/webservices/company/employee-account",
                        transformRequest: angular.identity,
                        headers: {
                            "Content-Type": undefined
                        },
                        params: {
                            "files": self.EmiratesId,
                            "document-type": "1",
                            "uuid": self.uuid,
                            "employee-no": self.dependent.EmployeeNo,
                            "mobile": $scope.formattedNumber,
                            "email": self.dependent.emailAddress,
                            "passport-no": self.dependent.PassportNumber,
                            "emirates-id": self.dependent.EID,
                            "name": self.dependent.FullName,
                            "nationality": "emirati",
                            "dob": self.FormattedDate,
                            "gender": self.dependent.gender,
                            "expiry": ""
                        },
                        data: fileData
                    }).then(function mySuccess(response) {
                        $scope.SubmitButtonText = "Success!";
                        $scope.SuccessMessage = "Your sub account has been added successfully";
                        $scope.FailiureMessage = "";

                    }, function myError(response) {
                        $scope.SuccessMessage = "";
                        $scope.SubmitButtonText = "Something went wrong";
                        $scope.FailiureMessage = response.data.errors[0].message;

                    })

                }
                else {
                    $scope.FailiureMessage = response.data.errors[0].message;

                }
            };


            /*  *********************************
              file uploading module starts here
              ***********************************/


            /*   ***************************************
               Code for country code and flag
               *****************************************/
            $("#mobileNumber").intlTelInput({
                setCountry: "uae",
                preferredCountries: ["ae", "sa"],
                separateDialCode: true
            });

            var countryData = $.fn.intlTelInput.getCountryData(),
                telInput = $("#mobileNumber");
            telInput.intlTelInput({
                utilsScript: "./../assets/js/utils.js" // just for formatting/placeholders etc
            });

            $("#mobileNumber").on("countrychange", function (e, countryData) {
                self.dependent.countryCode = countryData.dialCode;
            });

            $("#mobileNumber").on("change", function () {
                self.dependent.countryCode = $("#mobileNumber").intlTelInput("getSelectedCountryData").dialCode;
            });


            /*****************************
             HTTP CALL TO API STARTS HERE
             *****************************/

        }])
;