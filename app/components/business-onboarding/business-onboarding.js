"use strict";

myApp.controller("CompanyController", ["$scope", "$http", "$location", "ApiUrl", function ($scope, $http, $location, ApiUrl) {

    var uuid;
    var self = this;
    self.user = {};
    self.company = {};
    self.step = 0;
    self.company.entityType = "COMPANY";
    self.company.countryCode = "";
    $scope.ButtonText = "JOIN NOW";
    $scope.ErrorMessage = "";
    $scope.userLoggedIn = localStorage.getItem("USER_LOGGED_IN");

    if (!$scope.userLoggedIn) {

        $("#mobileNumber").intlTelInput({
            setCountry: "uae",
            preferredCountries: ["ae", "sa"],
            separateDialCode: true
        });

        var countryData = $.fn.intlTelInput.getCountryData(),
            telInput = $("#mobileNumber");
        telInput.intlTelInput({
            utilsScript: "./../assets/js/utils.js" // just for formatting/placeholders etc
        });

        $("#mobileNumber").on("countrychange", function (e, countryData) {
            self.company.countryCode = countryData.dialCode;
        });

        $("#mobileNumber").on("change", function () {
            self.company.countryCode = $("#mobileNumber").intlTelInput("getSelectedCountryData").dialCode;
        });

        self.directTo__home = function () {
            $location.path("/business-signin");
        };

        self.directTo__SignIn = function () {
            $location.path("/business-signin");
        };

        self.gotToStep = function (stepNumber) {
            self.step = stepNumber;
        };

        self.step1 = function () {
            $scope.ErrorMessage = "";

            // check validations here
            if (!self.company.myEmail || self.company.myEmail.toString().trim() == "") {
                $scope.ErrorMessage = "Email is required";
                return;
            } else if (!self.company.myEmail.match(/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/)) {
                $scope.ErrorMessage = "Invalid email address";
                return;
            }

            $scope.ButtonText = "Please wait...";
            $http({
                method: "POST",
                url: ApiUrl + "/webservices/signUp/verify-email",
                headers: {"Content-Type": "application/json"},
                params: {
                    "email": self.company.myEmail,
                    "entity-type": "COMPANY"
                }
            }).then(function mySuccess(response) {
                $scope.ButtonText = "JOIN NOW";
                self.step = 2;
                $scope.$emit("eventName", {message: 2});
            }, function myError(response) {
                $scope.ButtonText = "Try again";
                console.error(response);
                if (response.status == 400 && response.data.data == null && response.data.errors[0].code) {
                    self.step = 20;
                    uuid = response.data.errors[0].message;

                    $http({
                        method: "GET",
                        url: ApiUrl + "/webservices/utility/send-otp",
                        headers:
                            {
                                "Content-Type": "application/json"
                            },
                        params: {
                            "email": self.company.myEmail,
                            "entity-type": self.company.entityType,
                            "uuid": uuid
                        }
                    }).then(function mySuccess(response) {
                        if (response) {
                            $scope.formattedNumber = response.data.data;
                        }
                        self.step = 21;

                    }, function myError(response) {
                        console.error(response);
                    });
                }
                if (response.data && response.data.errors && response.data.errors[0]) {
                    $scope.ErrorMessage = response.data.errors[0].message;
                }
            });
        };

        self.step2 = function () {
            $scope.ErrorMessage = "";

            if (!self.company.companyName || self.company.companyName.toString().trim() == "") {
                $scope.ErrorMessage = "Company name is required.";
                return;
            } else if (self.company.companyName.toString().length < 3 || self.company.companyName.toString().length > 25) {
                $scope.ErrorMessage = "Company name should be 3 to 25 characters long.";
                return;
            }

            $http({
                method: "GET",
                url: ApiUrl + "/webservices/signUp/verify-company-name/",
                headers: {"Content-Type": "application/json"},
                params: {
                    "company-name": self.company.companyName
                }
            }).then(function mySuccess(response) {
                self.step = 3;
            }, function myError(response) {
                console.error(response);
                if (response.data && response.data.errors && response.data.errors[0]) {
                    $scope.ErrorMessage = response.data.errors[0].message;
                }
            });

        };

        self.step3 = function () {
            $scope.ErrorMessage = "";

            if (!self.company.firstName || self.company.firstName.toString().trim() == "") {
                $scope.ErrorMessage = "First name is required.";
                return;
            } else if (self.company.firstName.toString().length < 3 || self.company.firstName.toString().length > 25) {
                $scope.ErrorMessage = "First name should be 3 to 25 characters long.";
                return;
            } else if (!self.company.lastName || self.company.lastName.toString().trim() == "") {
                $scope.ErrorMessage = "Last name is required.";
                return;
            } else if (self.company.lastName.toString().length < 3 || self.company.lastName.toString().length > 25) {
                $scope.ErrorMessage = "Last name should be 3 to 25 characters long.";
                return;
            }

            self.step = 4;
        };

        self.step4 = function () {
            $scope.ErrorMessage = "";

            var password = self.company.myPassword;
            var cPassword = self.company.confirmPassword;

            if (!password || password.toString().trim() == "") {
                $scope.ErrorMessage = "Password is required.";
                return;
            } else if (password.toString().length < 8 || password.toString().length > 16) {
                $scope.ErrorMessage = "Password should be 8 to 16 characters long.";
                return;
            } else if (!cPassword || cPassword.toString().trim() == "") {
                $scope.ErrorMessage = "Confirm password is required.";
                return;
            } else if (cPassword.toString().length < 8 || cPassword.toString().length > 16) {
                $scope.ErrorMessage = "Confirm password should be 8 to 16 characters long.";
                return;
            } else if (password !== cPassword) {
                $scope.ErrorMessage = "Password do not match.";
                return;
            } else if (!password.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/)) {
                $scope.ErrorMessage = "Password must include 8 min alphanumeric including 1 capital and 1 digit.";
                return;
            }

            self.step = 5;
        };

        self.step5 = function () {
            $scope.ErrorMessage = "";

            var mob = $("#mobileNumber").val();

            if (!mob || mob.trim().length == 0) {
                $scope.ErrorMessage = "Mobile no. is required.";
                return;
            } else if (!mob.match(/^[0-9]{1,20}$/)) {
                $scope.ErrorMessage = "Invalid mobile number.";
                return;
            }

            $scope.formattedNumber = "+" + self.company.countryCode + parseInt(mob);

            $http({
                method: "POST",
                url: ApiUrl + "/webservices/signUp/company/",
                headers: {"Content-Type": "application/json"},
                params: {
                    "email": self.company.myEmail,
                    "first-name": self.company.firstName,
                    "last-name": self.company.lastName,
                    "company-name": self.company.companyName,
                    "password": self.company.myPassword,
                    "mobile-no": $scope.formattedNumber
                }
            }).then(function mySuccess(response) {
                self.company.uuid = response.data.data;
                uuid = response.data.data;
                self.step = 6;
            }, function myError(response) {
                console.error(response);
                if (response.data && response.data.errors && response.data.errors[0]) {
                    $scope.ErrorMessage = response.data.errors[0].message;
                }
            });
        };

        self.step6 = function () {
            $scope.ErrorMessage = "";

            if (!self.company.OTP || self.company.OTP.toString().trim() == "") {
                $scope.ErrorMessage = "OTP is required.";
                return;
            }

            $http({
                method: "POST",
                url: ApiUrl + "/webservices/signUp/verify-otp",
                headers:
                    {
                        "Content-Type": "application/json"
                    },
                params: {
                    "uuid": uuid,
                    "otp": self.company.OTP,
                    "entity-type": self.company.entityType

                }
            }).then(function mySuccess(response) {
                if (response.data) {
                    var token = response.data;
                    $location.path("/business-signin");
                    // $http({
                    //     method: "POST",
                    //     url: ApiUrl + "/webservices/utility/user-data",
                    //     headers: {"Content-Type": "application/json"},
                    //     params: {
                    //         "uuid": uuid
                    //     }
                    // }).then(function mySuccess(response2) {
                    //     self.user = response2.data.user;
                    //     localStorage.setItem("JWT_TOKEN", token);
                    //     localStorage.setItem("USER_LOGGED_IN", true);
                    //     localStorage.setItem("USER_UUID", self.user.uuid);
                    //     localStorage.setItem("USER_FULL_NAME", self.user.userDetail.firstName + " " + self.user.userDetail.lastName);
                    //     localStorage.setItem("USER_MOBILE_NUMBER", self.user.userDetail.mobileNo);
                    //     $location.path("/secure/company-dashboard");
                    // }, function myError(response2) {
                    //     console.error(response);
                    //     if (response.data && response.data.errors && response.data.errors[0]) {
                    //         $scope.ErrorMessage = response.data.errors[0].message;
                    //     }
                    // });
                }

            }, function myError(response) {
                $scope.ErrorMessage = "OTP is invalid. Try again.";
            });
        };

        self.resendOTP = function () {
            $scope.ErrorMessage = "";

            $http({
                method: "GET",
                url: ApiUrl + "/webservices/utility/resend-otp",
                headers:
                    {
                        "Content-Type": "application/json"
                    },
                params: {
                    "mobile-no": $scope.formattedNumber,
                    "entity-type": self.company.entityType,
                    "uuid": uuid
                }
            }).then(function mySuccess(response) {
                $scope.ErrorMessage = "A new OTP has been sent. Please check your device.";
            }, function myError(response) {
                console.error(response);
            });
        };

        self.verifyResumedOTP = function (isValid) {
            $scope.ErrorMessage = "";

            if (isValid) {
                $http({
                    method: "POST",
                    url: ApiUrl + "/webservices/signUp/verify-otp",
                    headers:
                        {
                            "Content-Type": "application/json"
                        },
                    params: {
                        "uuid": uuid,
                        "otp": self.company.OTP,
                        "entity-type": self.company.entityType
                    }
                }).then(function mySuccess(response) {

                }, function myError(response) {
                    $scope.OtpError = "OTP is invalid. Try again.";
                });
            }
        };
    }
}]);