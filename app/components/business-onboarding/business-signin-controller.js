"use strict";

myApp.controller("BusinessSigninController", ["$scope", "$http", "$location", "ApiUrl", "$rootScope",
    function ($scope, $http, $location, ApiUrl, $rootScope) {
        var self = this;
        self.login = {};
        self.step = 1;
        self.user = {};

        self.name = localStorage.setItem("USER_FULL_NAME", self.login.username);

        self.redirectTo__SignUp = function () {
            $location.path("/coming-soon");
        };
        self.Signin = function () {
            if (self.login.username && self.login.password) {
                $rootScope.currentUserSignedIn = true;
                localStorage.setItem("USER_LOGGED_IN", true);
                self.userName = localStorage.setItem("USER_FULL_NAME",self.login.username);
                self.emailAddress = localStorage.getItem("EMAIL_ADDRESS");
                alert(self.login.username);
                $location.path('/user-dashboard');
            }
        }
    }]);
