"use strict";

myApp.controller("ServiceController", ["$scope", "$location", "ApiUrl", "$http", function ($scope, $location, ApiUrl, $http) {
    var self = this;

    self.step = 1;
    self.signup = {};
    self.login = {};

    $scope.names = ["Lahore", "Multan", "Islamabad", "Karachi"];

    self.direct_to_form = function () {
        self.step = 2;
    };

    self.SubmitData = function () {
        $scope.SuccessMessage = "Thank you. We will contact you shortly on the given contact information"
    };

}]);
