"use strict";

myApp.controller("404Controller", ["$scope", function ($scope) {
    $scope.message = "The resource you have requested is not found.";
}]);