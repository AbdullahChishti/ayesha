"use strict";

myApp.controller("ComingSoonController", ["$scope","$location", function ($scope, $location) {
    var self = this;

    $scope.names = ["Lahore", "Multan", "Islamabad", "Karachi"];
    self.SubmitData = function () {
        $scope.SuccessMessage = "Your account has been successfully created. Please login with the given email and password";
        alert("you will now be directed to the home page");
        $location.path('/partners');

    };
}]);