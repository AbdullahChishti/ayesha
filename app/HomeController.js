"use strict";

myApp.controller("HomeController", ["$scope", "$rootScope", "$location", "$timeout",
    function ($scope, $rootScope, $location, $timeout) {

        var self = this;
        $rootScope.count = self.enabler;
        $rootScope.currentUserSignedIn = false;
        $scope.userLoggedIn = localStorage.getItem("USER_LOGGED_IN");
        $scope.UserType = localStorage.getItem("ENTITY_TYPE");
        if ($scope.userLoggedIn === "true") {
            $rootScope.currentUserSignedIn = true;
            $scope.userName = localStorage.getItem("USER_FULL_NAME");
            $scope.email = localStorage.getItem("EMAIL_ADDRESS");
        }

        // $scope.$watch('a', function (newVal, OldVal) {
        //     $scope.email = localStorage.getItem("EMAIL_ADDRESS");
        //     $scope.userName = localStorage.getItem("USER_FULL_NAME");
        // });

        $timeout(function () {
            $scope.$watch(function (oldVal, NewVal) {
                $scope.email = localStorage.getItem("EMAIL_ADDRESS");
                $scope.userName = localStorage.getItem("USER_FULL_NAME");
                $scope.UserType = localStorage.getItem("ENTITY_TYPE");
            });
        }, 1000);

        self.enabler = 1;
        $scope.$on("eventName", function (event, args) {
            self.enabler =
                args.message;
        });
        $scope.$on("MyEvent", function (event, args) {
            self.enabler = args.message;
        });

        self.direct_to_Individual = function () {
            self.enabler = 1;
            $location.path("/secure/customer-onboarding");
        };
        self.directTo__Signup = function () {
            if (!$scope.userLoggedIn) {
                $location.path("/");
            }
        };

        self.directTo__SignIn = function () {
            $location.path("/business-signin");
        };

        self.directTo__Customer = function () {
            $location.path("/secure/customer-onboarding");
        };

        self.directTo__CompanyDashboard = function () {
            $location.path('/user-dashboard');
        };

        self.directTo__CompanyActivity = function () {
            $location.path("/secure/company-activity");
        };

        self.directTo__CompanyTransactions = function () {
            $location.path("/secure/company-transactions");
        };

        self.directTo__CompanyExpenseAccounts = function () {
            $location.path("/secure/company-expense-accounts");
        };

        self.directTo__CompanyPayrollAccounts = function () {
            $location.path("/secure/payroll-landing-screen");
        };

        self.directTo__CompanyReports = function () {
            $location.path("/secure/company-reports");
        };

        self.directTo__ComingSoon = function () {
            $location.path("/404");
        };

        self.directTo__HomePage = function () {
            $location.path("/Homepage");
        };

        self.directTo__Partners = function () {
            $location.path("/partners");
        };


        self.logout = function () {
            localStorage.removeItem("USER_LOGGED_IN");
            localStorage.removeItem("USER_FULL_NAME");
            localStorage.removeItem("USER_UUID");
            localStorage.removeItem("USER_MOBILE_NUMBER");
            localStorage.removeItem("JWT_TOKEN");
            $rootScope.currentUserSignedIn = false;
            $location.path("/");
        };
    }]);